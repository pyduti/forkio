const  gulp = require("gulp"),
    concat = require('gulp-concat'),
    clean = require('gulp-clean'),
    browserSync = require('browser-sync').create(),
    imagemin = require('gulp-imagemin'),
    sass = require('gulp-sass');
    sass.compiler = require('node-sass');
    uglify = require('gulp-uglify'),
    pipeline = require('readable-stream').pipeline,
    minifyjs = require('gulp-js-minify'),
    cleanCSS = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer');

/***                        PATHS                        ***/
const paths = {
    src: {
        scss: './src/scss/*.scss',
        js: './src/js/*.js',
        img: './src/img/*'
    },
    dist: {
        css: './dist/css',
        js: './dist/js',
        img: './dist/img',
        self: './dist/'
    }
}
/***                        PATHS                        ***/

/***    FUNCTIONS    ***/
const buildSCSS = () => (
    gulp.src(paths.src.scss)
        .pipe(sass().on('error', sass.logError))
            .pipe(gulp.dest(paths.dist.css))
                .pipe(browserSync.stream())
)

const buildJS = () => (
    gulp.src(paths.src.js)
    .pipe(concat('scripts.min.js'))
    .pipe(gulp.dest(paths.dist.js))
    .pipe(browserSync.stream())
)

const buildImg = () => (
    gulp.src(paths.src.img)
    .pipe(gulp.dest(paths.dist.img))
    .pipe(browserSync.stream())
)


const cleanBuild = () => (
    gulp.src(paths.dist.self, { allowEmpty: true })
    .pipe(clean())
);

const watcher = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch(paths.src.scss, buildSCSS).on("change", browserSync.reload);
    gulp.watch(paths.src.js, buildJS).on("change", browserSync.reload);
    gulp.watch(paths.src.img, buildImg).on("change", browserSync.reload);
};

const imgMin = () => (
    gulp.src(paths.src.img)
        .pipe(imagemin())
            .pipe(gulp.dest(paths.dist.img))
);


const compresedJS = function () {
          return pipeline(
              gulp.src(paths.dist.js),
                uglify(),
                    gulp.dest(paths.dist.js));
};

const minifyJS = () => (
    gulp.src(paths.dist.js)
      .pipe(minifyjs())
      .pipe(gulp.dest(paths.dist.js))
  );

const minifyCSS = () => (
    gulp.src(paths.dist.css)
      .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(paths.dist.css))
);

const autopref = () => (
	gulp.src(paths.dist.css)
		.pipe(autoprefixer({
			cascade: false
		}))
		    .pipe(gulp.dest(paths.dist.css))
);

/***    TASK    ***/

gulp.task('default', gulp.series(
    cleanBuild,
    imgMin,
    buildSCSS,
    buildJS,
    buildImg,
    autopref,
    minifyCSS,
    buildJS,
    compresedJS,
    minifyJS,
    watcher
));